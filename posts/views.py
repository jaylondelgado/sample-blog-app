from django.views.generic import ListView, DetailView
from django.views.generic.edit import CreateView, UpdateView

try:
    from posts.models import Post
except Exception:
    Post = None


class PostListView(ListView):
    model = Post
    context_object_name = "posts"
    template_name = "posts/list.html"
    paginate_by = 3

    def get_queryset(self):
        query = self.request.GET.get("q")
        if not query:
            query = ""
        return Post.objects.filter(title_icontains=query)


class PostDetailView(DetailView):
    model = Post
    context_object_name = "post"
    template_name = "posts/detail.html"


class PostCreateView(CreateView):
    model = Post
    fields = ["title", "content", "created_at"]
    template_name = "posts/create.html"
    success_url = "/"


class PostUpdateView(UpdateView):
    model = Post
    fields = ["title", "content", "create_at"]
    template_name = "posts/update.html"
    success_url = "/"
